function police(){
    var nbParagraphe = document.getElementsByTagName("p").length;

    for(var i = 0; i < nbParagraphe; i++){
        document.getElementsByTagName("p")[i].style.fontFamily = "Arial";
    }
}

function ordre() {
    var p1 = document.getElementById("p1").innerHTML;
    var p2 = document.getElementById("p2").innerHTML;
    var p3 = document.getElementById("p3").innerHTML;

    document.getElementById("p3").innerHTML = p1;
    document.getElementById("p1").innerHTML = p2;
    document.getElementById("p2").innerHTML = p3;
}

function afficherTitre() {
    var titre = document.getElementById("titre");
    //console.log permet d'afficher dans l'onglet console de l'inspecteur d'élément/dev tools. Très utile pour débugger.
    //permet de voir que la 1ere valeur de visibility est une chaine vide
    console.log("visibilité: " + titre.style.visibility);

    if( titre.style.visibility === "" || titre.style.visibility === "visible"){
        titre.style.visibility = "hidden";
    } else{
        titre.style.visibility = "visible";
    }
}

function rouge() {
    document.getElementById("titre").style.color = "red";
}

function ajouterParagraphe() {
    var contenu =   "Sed accumsan magna quam, in pellentesque urna pulvinar quis. Ut iaculis condimentum ex in tristique. Nulla egestas lorem";
    var paragraphe = document.createElement("p");
    paragraphe.innerHTML = contenu;
    document.body.append(paragraphe);
}