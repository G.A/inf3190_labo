function resultat() {
    var cours = document.getElementById("cours").value;

    var xmlhttp = new XMLHttpRequest();
    //Lorsque l'état de l'objet xmlhttp change, on écrit
    //une fonction qui récupère le résultat de la requête.
    //readyState 4 signifie que la requête est prête et status 200 signifie OK.
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            document.getElementById("suggestion").innerHTML = xmlhttp.responseText;
        }
    };

    //requête HTTP. Contrairement à l'étape PHP où le contenu du input était envoyé dans l'url avec ?cours=INF,
    //en inspectant la page (click droit -> inspect) on peut voir que la valeur du input est envoyé dans l'attribut q.
    xmlhttp.open("GET", "../PHP/recherche.php?q=" + cours, true);
    //envoi de la requête
    xmlhttp.send();
}