<?php
$fichier = fopen("../Static/cours.txt","r");
//explode permet de récupérer le contenu d'un fichier séparé par un délimiteur(fonctionne aussi pour les fichiers csv)
$listeCours = explode("\n",fread($fichier,filesize("../Static/cours.txt")));
fclose($fichier);

//request récupère le paramètre envoyé. Sur votre page -> click droit -> inspect -> Network
//vous pourrez voir que le serveur envoi le contenu du input dans l'attribut q.
$q = $_REQUEST["q"];

if($q != ""){
    $q = strtoupper($q);
    foreach ($listeCours as $cours){
        //stristr vérifie si q se retrouve dans cours (est-ce que INF est dans INF1120?)
        if(stristr($cours, $q)){
            echo $cours;
        }
    }
}
