<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Labo Ajax</title>
</head>
<body>
<h1>Rechercher un cours</h1>
<form action="index.php" method="get">
    <label for="cours">Titre du cours</label>
    <input type="text" id="cours" name="cours">
    <button type="submit">Recherche</button>
</form>
</body>
</html>
<?php
if($_SERVER["REQUEST_METHOD"] == 'GET'){
    if(!isset($_GET["cours"])){
        http_response_code(400);
        exit;
    }
    $fichier = fopen("Static/cours.txt","r");
    //explode permet de récupérer le contenu d'un fichier séparé par un délimiteur(fonctionne aussi pour les fichiers csv)
    $listeCours = explode("\n",fread($fichier,filesize("Static/cours.txt")));
    fclose($fichier);

    $q = $_REQUEST["cours"];
    $suggestion = array();
    if($q != ""){
        $q = strtoupper($q);
        foreach ($listeCours as $cours){
            //stristr vérifie si q se retrouve dans cours (est-ce que INF est dans INF1120?)
            if(stristr($cours,$q)){
                array_push($suggestion, $cours);
            }
        }
    }
    echo "<p>Suggestion: </p>";
    foreach ($suggestion as $cours){
        echo $cours;
    }
}
?>
