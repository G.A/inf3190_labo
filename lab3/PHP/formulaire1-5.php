<?php

/*
Message d'erreur général
*/

if (verificationDonnees()) {
    echo "Le formulaire est complet.";
} else {
    echo "Une erreur s'est glissée dans les données.";
}

function verificationDonnees()
{
    $nomValide = $prenomValide = $anneeValide = $ageValide = $universiteValide = $programmeValide = false;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (!isset($_POST["nom"]) || !isset($_POST["prenom"]) || !isset($_POST["age"]) || !isset($_POST["anneeGraduation"])
            || !isset($_POST["universite"]) || !isset($_POST["programme"])) {
            http_response_code(400);
            exit;
        }
        $nom = $_POST["nom"];
        $prenom = $_POST["prenom"];
        $age = $_POST["age"];
        $annee = $_POST["anneeGraduation"];
        $universite = $_POST["universite"];
        $programme = $_POST["programme"];

        if (!(empty($nom) && empty($prenom) && empty($age) && empty($annee) && empty($universite) && empty($programme))) {
            $nomValide = (strlen($nom) <= 40);
            $prenomValide = (strlen($prenom) <= 50);
            $ageValide = (strlen($age) <= 2) && (is_numeric($age));
            $anneeValide = (strlen($annee) == 4) && (is_numeric($annee));
            $universiteValide = true;
            $programmeValide = true;
        }
    }
    return $nomValide && $prenomValide && $ageValide && $universiteValide && $programmeValide && $anneeValide;
}

?>