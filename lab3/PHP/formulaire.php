<?php

/*
Message significatif pour chaque erreur
*/

if(verificationDonnees()){
    echo "Le formulaire est complet.";
}

function verificationDonnees(){

    $prenomValide = $nomValide = $ageValide = $anneeGraduationValide = $programmeValide = $universiteValide = false;

    if($_SERVER["REQUEST_METHOD"] == "POST"){

            $nomValide = nomValide();
            $prenomValide = prenomValide();
            $ageValide = ageValide();
            $anneeGraduationValide = anneeValide();
            $universiteValide = universiteValide();
            $programmeValide =  programmeValide();


    }
    return $prenomValide && $nomValide && $ageValide && $anneeGraduationValide && $programmeValide && $universiteValide;
}

function nomValide(){
    $nomEstValide = true;
    if(!isset($_POST["nom"])){
        http_response_code(400);
        exit;
    }
    $nom = $_POST["nom"];

    if((empty($nom))){
        echo "<p>Le champ nom est obligatoire</p>";
        $nomEstValide = false;
    }
    if(strlen($nom) > 40){
        echo "<p>Le champ nom ne doit pas dépasser 40 caractères</p>";
        $nomEstValide = false;
    }
    return $nomEstValide;
}

function prenomValide(){
    $prenomEstValide = true;
    if(!isset($_POST["prenom"])){
        http_response_code(400);
        exit;
    }
    $prenom = $_POST["prenom"];

    if((empty($prenom))){
        echo "<p>Le champ prénom est obligatoire</p>";
        $prenomEstValide = false;
    }
    if(strlen($prenom) > 50){
        echo "<p>Le champ prénom ne doit pas dépasser 50 caractères</p>";
        $prenomEstValide = false;
    }
    return $prenomEstValide;
}

function ageValide(){
    $ageEstValide = true;
    if(!isset($_POST["age"])){
        http_response_code(400);
        exit;
    }
    $age = $_POST["age"];

    if(empty($age)){
        echo "<p>Le champ âge est obligatoire</p>";
        $ageEstValide = false;
    }
    if(strlen($age) > 2 || !is_numeric($age)){
        echo "<p> L'âge doit être composé d'au moins un chiffres.</p>";
        $ageEstValide = false;
    }
    return $ageEstValide;
}

function anneeValide(){
    $anneeEstValide = true;
    if(!isset($_POST["anneeGraduation"])){
        http_response_code(400);
        exit;
    }
    $annee = $_POST["anneeGraduation"];

    if(empty($annee)){
        echo "<p>Le champ année est obligatoire</p>";
        $anneeEstValide = false;
    }
    if(strlen($annee) != 4 || !is_numeric($annee)){
        echo  "<p>L'année doit être composée de quatre chiffres</p>";
        $anneeEstValide = false;
    }
    return $anneeEstValide;
}
function universiteValide(){
    $universiteEstValide = true;

    if(!isset($_POST["universite"])){
        http_response_code(400);
        exit;
    }
    $universite = $_POST["universite"];

    if(empty($universite)){
        echo "<p>Le choix d'université est obligatoire</p>";
        $universiteEstValide = false;
    }

    return $universiteEstValide;
}

function programmeValide(){
    $programmeEstValide = true;
    if(!isset($_POST["programme"])){
        http_response_code(400);
        exit;
    }
    $programme = $_POST["programme"];
    if(empty($programme)){
        echo "<p>Le champ programme est obligatoire</p>";
        $programmeEstValide = false;
    }
    return $programmeEstValide;
}
?>